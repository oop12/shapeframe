/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.soi.shapeframe;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

/**
 *
 * @author user
 */
public class TriangleFrame extends JFrame {
    
    JLabel lblBase;
    JTextField txtBase;
    JLabel lblHeight;
    JTextField txtHeight;
    JButton btnCalArea;
    JLabel lblResult;
    
    public TriangleFrame() {
        super("Triangle");
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setSize(500, 300);
        this.setLayout(null);
        
        lblBase = new JLabel("base : ", JLabel.TRAILING);
        lblBase.setSize(45, 20);
        lblBase.setLocation(5, 5);
        lblBase.setBackground(Color.PINK);
        lblBase.setOpaque(true);
        this.add(lblBase);
        
        txtBase = new JTextField();
        txtBase.setSize(50, 20);
        txtBase.setLocation(60, 5);
        this.add(txtBase);
        
        lblHeight = new JLabel("height : ", JLabel.TRAILING);
        lblHeight.setSize(50, 20);
        lblHeight.setLocation(120, 5);
        lblHeight.setBackground(Color.PINK);
        lblHeight.setOpaque(true);
        this.add(lblHeight);
        
        txtHeight = new JTextField();
        txtHeight.setSize(50, 20);
        txtHeight.setLocation(180, 5);
        this.add(txtHeight);
        
        btnCalArea = new JButton("Calculate");
        btnCalArea.setSize(100, 20);
        btnCalArea.setLocation(240, 5);
        this.add(btnCalArea);
        
        lblResult = new JLabel("Triangle base = ???, height = ??? Area = ??? Perimeter = ???");
        lblResult.setHorizontalAlignment(JLabel.CENTER);
        lblResult.setSize(500, 80);
        lblResult.setLocation(0, 50);
        lblResult.setBackground(Color.WHITE);
        lblResult.setOpaque(true);
        this.add(lblResult);
        
        btnCalArea.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    String strBase = txtBase.getText();
                    String strHeight = txtHeight.getText();
                    
                    double base = Double.parseDouble(strBase);
                    double height = Double.parseDouble(strHeight);
                    
                    Triangle triangle = new Triangle(base, height);
                    
                    lblResult.setText("Triangle base = " + String.format("%.2f", triangle.getBase())
                            + ", height = " + triangle.getHeight()
                            + " Area = " + String.format("%.2f", triangle.calArea())
                            + " Perimeter = " + String.format("%.2f", triangle.calPerimeter()));
                    
                } catch (Exception ex) {
                    JOptionPane.showMessageDialog(TriangleFrame.this, "Error: Please input number", "ERROR",
                            JOptionPane.ERROR_MESSAGE);
                    txtBase.setText("");
                    txtHeight.setText("");
                    txtBase.requestFocus();
                    
                }
            }
        });
        
    }
    
    public static void main(String[] args) {
        TriangleFrame frame = new TriangleFrame();
        frame.setVisible(true);
    }
    
}
