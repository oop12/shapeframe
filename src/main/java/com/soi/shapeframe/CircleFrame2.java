/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.soi.shapeframe;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

/**
 *
 * @author user
 */
public class CircleFrame2 extends JFrame {

    JLabel lblRadius;
    JTextField txtRadius;
    JButton btnCalculate;
    JLabel lblResult;

    public CircleFrame2() {
        super("Circle");
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setSize(500, 300);
        this.setLayout(null);

        lblRadius = new JLabel("radius : ", JLabel.TRAILING);
        lblRadius.setSize(50, 20);
        lblRadius.setLocation(5, 5);
        lblRadius.setBackground(Color.PINK);
        lblRadius.setOpaque(true);
        this.add(lblRadius);

        txtRadius = new JTextField();
        txtRadius.setSize(50, 20);
        txtRadius.setLocation(60, 5);
        this.add(txtRadius);

        btnCalculate = new JButton("Calculate");
        btnCalculate.setSize(100, 20);
        btnCalculate.setLocation(120, 5);
        this.add(btnCalculate);

        lblResult = new JLabel("Circle radius = ??? Area = ??? Perimeter = ???");
        lblResult.setHorizontalAlignment(JLabel.CENTER);
        lblResult.setSize(500, 80);
        lblResult.setLocation(0, 50);
        lblResult.setBackground(Color.WHITE);
        lblResult.setOpaque(true);
        this.add(lblResult);

        btnCalculate.addActionListener(new ActionListener() { //Anonymous class
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    //1.ดึงข้อมมูลจาก text >> txtRadius -> strRadius
                    String strRadius = txtRadius.getText();
                    //2.แปลง strRadius -> radius : double.parseDouble
                    double radius = Double.parseDouble(strRadius);
                    //3.Intance obj Circle(radius)
                    Circle circle = new Circle(radius);
                    //4. update lblResult นำข้อมูลไปใส่
                    lblResult.setText("Circle radius = " + String.format("%.2f", circle.getRadius())
                            + " Area = " + String.format("%.2f", circle.calArea())
                            + " Perimeter = " + String.format("%.2f", circle.calPerimeter()));
                } catch (Exception ex) {
                    JOptionPane.showMessageDialog(CircleFrame2.this, "Error: Please input number", "ERROR",
                            JOptionPane.ERROR_MESSAGE);
                    txtRadius.setText("");
                    txtRadius.requestFocus();
                }
            }

        });
    }

    public static void main(String[] args) {
        CircleFrame2 frame = new CircleFrame2();
        frame.setVisible(true);

    }

}
