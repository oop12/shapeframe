/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.soi.shapeframe;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

/**
 *
 * @author user
 */
public class RectangleFrame extends JFrame {

    JLabel lblWidth;
    JTextField txtWidth;
    JLabel lblHeight;
    JTextField txtHeight;
    JButton btnCalArea;
    JLabel lblResult;

    public RectangleFrame() {
        super("Rectangle");
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setSize(500, 300);
        this.setLayout(null);

        lblWidth = new JLabel("width : ", JLabel.TRAILING);
        lblWidth.setSize(45, 20);
        lblWidth.setLocation(5, 5);
        lblWidth.setBackground(Color.PINK);
        lblWidth.setOpaque(true);
        this.add(lblWidth);

        txtWidth = new JTextField();
        txtWidth.setSize(50, 20);
        txtWidth.setLocation(60, 5);
        this.add(txtWidth);

        lblHeight = new JLabel("height : ", JLabel.TRAILING);
        lblHeight.setSize(50, 20);
        lblHeight.setLocation(120, 5);
        lblHeight.setBackground(Color.PINK);
        lblHeight.setOpaque(true);
        this.add(lblHeight);

        txtHeight = new JTextField();
        txtHeight.setSize(50, 20);
        txtHeight.setLocation(180, 5);
        this.add(txtHeight);

        btnCalArea = new JButton("Calculate");
        btnCalArea.setSize(100, 20);
        btnCalArea.setLocation(240, 5);
        this.add(btnCalArea);

        lblResult = new JLabel("Rectangle width = ???, height = ??? Area = ??? Perimeter = ???");
        lblResult.setHorizontalAlignment(JLabel.CENTER);
        lblResult.setSize(500, 80);
        lblResult.setLocation(0, 50);
        lblResult.setBackground(Color.WHITE);
        lblResult.setOpaque(true);
        this.add(lblResult);

        btnCalArea.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    String strWidth = txtWidth.getText();
                    String strHString = txtHeight.getText();

                    double width = Double.parseDouble(strWidth);
                    double height = Double.parseDouble(strHString);

                    Rectangle rectangle = new Rectangle(width, height);

                    lblResult.setText("Rectangle width = " + String.format("%.2f", rectangle.getWidth())
                            + ", height = " + rectangle.getHeight()
                            + " Area = " + String.format("%.2f", rectangle.calArea())
                            + " Perimeter = " + String.format("%.2f", rectangle.calPerimeter()));
                } catch (Exception ex) {
                    JOptionPane.showMessageDialog(RectangleFrame.this, "Error: Please input number", "ERROR",
                            JOptionPane.ERROR_MESSAGE);
                    txtWidth.setText("");
                    txtHeight.setText("");
                    txtWidth.requestFocus();

                }
            }
        });
    }

    public static void main(String[] args) {
        RectangleFrame frame = new RectangleFrame();
        frame.setVisible(true);
    }

}
